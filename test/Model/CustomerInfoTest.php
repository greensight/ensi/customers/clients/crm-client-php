<?php
/**
 * CustomerInfoTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\CrmClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * CRM
 *
 * Управление клиентами
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace Ensi\CrmClient;

use PHPUnit\Framework\TestCase;

/**
 * CustomerInfoTest Class Doc Comment
 *
 * @category    Class
 * @description CustomerInfo
 * @package     Ensi\CrmClient
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class CustomerInfoTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "CustomerInfo"
     */
    public function testCustomerInfo()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "avatar"
     */
    public function testPropertyAvatar()
    {
    }

    /**
     * Test attribute "customer_id"
     */
    public function testPropertyCustomerId()
    {
    }

    /**
     * Test attribute "kpi_sku_count"
     */
    public function testPropertyKpiSkuCount()
    {
    }

    /**
     * Test attribute "kpi_sku_price_sum"
     */
    public function testPropertyKpiSkuPriceSum()
    {
    }

    /**
     * Test attribute "kpi_order_count"
     */
    public function testPropertyKpiOrderCount()
    {
    }

    /**
     * Test attribute "kpi_shipment_count"
     */
    public function testPropertyKpiShipmentCount()
    {
    }

    /**
     * Test attribute "kpi_delivered_count"
     */
    public function testPropertyKpiDeliveredCount()
    {
    }

    /**
     * Test attribute "kpi_delivered_sum"
     */
    public function testPropertyKpiDeliveredSum()
    {
    }

    /**
     * Test attribute "kpi_refunded_count"
     */
    public function testPropertyKpiRefundedCount()
    {
    }

    /**
     * Test attribute "kpi_refunded_sum"
     */
    public function testPropertyKpiRefundedSum()
    {
    }

    /**
     * Test attribute "kpi_canceled_count"
     */
    public function testPropertyKpiCanceledCount()
    {
    }

    /**
     * Test attribute "kpi_canceled_sum"
     */
    public function testPropertyKpiCanceledSum()
    {
    }
}
