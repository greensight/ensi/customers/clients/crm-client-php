<?php

namespace Ensi\CrmClient;

class CrmClientProvider
{
    /** @var string[] */
    public static $apis = [
        '\Ensi\CrmClient\Api\BonusOperationsApi',
        '\Ensi\CrmClient\Api\CustomerFavoritesApi',
        '\Ensi\CrmClient\Api\CustomersInfoApi',
        '\Ensi\CrmClient\Api\PreferencesApi',
        '\Ensi\CrmClient\Api\ProductSubscribesApi',
    ];

    /** @var string[] */
    public static $dtos = [
        '\Ensi\CrmClient\Dto\BonusOperation',
        '\Ensi\CrmClient\Dto\BonusOperationFillableProperties',
        '\Ensi\CrmClient\Dto\BonusOperationReadonlyProperties',
        '\Ensi\CrmClient\Dto\BonusOperationResponse',
        '\Ensi\CrmClient\Dto\ClearCustomerFavoritesRequest',
        '\Ensi\CrmClient\Dto\ClearProductSubscribesRequest',
        '\Ensi\CrmClient\Dto\CreateBonusOperationRequest',
        '\Ensi\CrmClient\Dto\CreateCustomerFavoritesRequest',
        '\Ensi\CrmClient\Dto\CreateCustomerInfoRequest',
        '\Ensi\CrmClient\Dto\CreateProductSubscribeRequest',
        '\Ensi\CrmClient\Dto\CustomerFavorite',
        '\Ensi\CrmClient\Dto\CustomerFavoriteFillableProperties',
        '\Ensi\CrmClient\Dto\CustomerFavoriteReadonlyProperties',
        '\Ensi\CrmClient\Dto\CustomerInfo',
        '\Ensi\CrmClient\Dto\CustomerInfoFillableProperties',
        '\Ensi\CrmClient\Dto\CustomerInfoReadonlyProperties',
        '\Ensi\CrmClient\Dto\CustomerInfoResponse',
        '\Ensi\CrmClient\Dto\DeleteCustomerFavoritesRequest',
        '\Ensi\CrmClient\Dto\DeleteProductSubscribesRequest',
        '\Ensi\CrmClient\Dto\EmptyDataResponse',
        '\Ensi\CrmClient\Dto\Error',
        '\Ensi\CrmClient\Dto\ErrorResponse',
        '\Ensi\CrmClient\Dto\ErrorResponse2',
        '\Ensi\CrmClient\Dto\File',
        '\Ensi\CrmClient\Dto\ModelInterface',
        '\Ensi\CrmClient\Dto\PaginationTypeCursorEnum',
        '\Ensi\CrmClient\Dto\PaginationTypeEnum',
        '\Ensi\CrmClient\Dto\PaginationTypeOffsetEnum',
        '\Ensi\CrmClient\Dto\PatchBonusOperationRequest',
        '\Ensi\CrmClient\Dto\PatchCustomerInfoRequest',
        '\Ensi\CrmClient\Dto\Preference',
        '\Ensi\CrmClient\Dto\PreferenceReadonlyProperties',
        '\Ensi\CrmClient\Dto\ProductSubscribe',
        '\Ensi\CrmClient\Dto\ProductSubscribeFillableProperties',
        '\Ensi\CrmClient\Dto\ProductSubscribeReadonlyProperties',
        '\Ensi\CrmClient\Dto\ProductSubscribeResponse',
        '\Ensi\CrmClient\Dto\RequestBodyCursorPagination',
        '\Ensi\CrmClient\Dto\RequestBodyOffsetPagination',
        '\Ensi\CrmClient\Dto\RequestBodyPagination',
        '\Ensi\CrmClient\Dto\ResponseBodyCursorPagination',
        '\Ensi\CrmClient\Dto\ResponseBodyOffsetPagination',
        '\Ensi\CrmClient\Dto\ResponseBodyPagination',
        '\Ensi\CrmClient\Dto\SearchBonusOperationsRequest',
        '\Ensi\CrmClient\Dto\SearchBonusOperationsResponse',
        '\Ensi\CrmClient\Dto\SearchCustomerFavoritesRequest',
        '\Ensi\CrmClient\Dto\SearchCustomerFavoritesResponse',
        '\Ensi\CrmClient\Dto\SearchCustomersInfoRequest',
        '\Ensi\CrmClient\Dto\SearchCustomersInfoResponse',
        '\Ensi\CrmClient\Dto\SearchCustomersInfoResponseMeta',
        '\Ensi\CrmClient\Dto\SearchPreferencesRequest',
        '\Ensi\CrmClient\Dto\SearchPreferencesResponse',
        '\Ensi\CrmClient\Dto\SearchProductSubscribesRequest',
        '\Ensi\CrmClient\Dto\SearchProductSubscribesResponse',
    ];

    /** @var string */
    public static $configuration = '\Ensi\CrmClient\Configuration';
}
