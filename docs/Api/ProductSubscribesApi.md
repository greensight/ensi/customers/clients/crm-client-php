# Ensi\CrmClient\ProductSubscribesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**clearProductSubscribes**](ProductSubscribesApi.md#clearProductSubscribes) | **POST** /customers/product-subscribes:clear | Удаление всех товаров из подписок клиента
[**createProductSubscribe**](ProductSubscribesApi.md#createProductSubscribe) | **POST** /customers/product-subscribes | Создание объекта типа ProductSubscribe
[**deleteProductFromProductSubscribes**](ProductSubscribesApi.md#deleteProductFromProductSubscribes) | **POST** /customers/product-subscribes:delete-product | Удаление товара из списка подписок клиента
[**searchProductSubscribes**](ProductSubscribesApi.md#searchProductSubscribes) | **POST** /customers/product-subscribes:search | Поиск объектов типа ProductSubscribe



## clearProductSubscribes

> \Ensi\CrmClient\Dto\EmptyDataResponse clearProductSubscribes($clear_product_subscribes_request)

Удаление всех товаров из подписок клиента

Удаление всех товаров из подписок клиента

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\ProductSubscribesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$clear_product_subscribes_request = new \Ensi\CrmClient\Dto\ClearProductSubscribesRequest(); // \Ensi\CrmClient\Dto\ClearProductSubscribesRequest | 

try {
    $result = $apiInstance->clearProductSubscribes($clear_product_subscribes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductSubscribesApi->clearProductSubscribes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clear_product_subscribes_request** | [**\Ensi\CrmClient\Dto\ClearProductSubscribesRequest**](../Model/ClearProductSubscribesRequest.md)|  |

### Return type

[**\Ensi\CrmClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## createProductSubscribe

> \Ensi\CrmClient\Dto\ProductSubscribeResponse createProductSubscribe($create_product_subscribe_request)

Создание объекта типа ProductSubscribe

Создание объекта типа ProductSubscribe

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\ProductSubscribesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_product_subscribe_request = new \Ensi\CrmClient\Dto\CreateProductSubscribeRequest(); // \Ensi\CrmClient\Dto\CreateProductSubscribeRequest | 

try {
    $result = $apiInstance->createProductSubscribe($create_product_subscribe_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductSubscribesApi->createProductSubscribe: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_product_subscribe_request** | [**\Ensi\CrmClient\Dto\CreateProductSubscribeRequest**](../Model/CreateProductSubscribeRequest.md)|  |

### Return type

[**\Ensi\CrmClient\Dto\ProductSubscribeResponse**](../Model/ProductSubscribeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteProductFromProductSubscribes

> \Ensi\CrmClient\Dto\EmptyDataResponse deleteProductFromProductSubscribes($delete_product_subscribes_request)

Удаление товара из списка подписок клиента

Удаление товара из списка подписок клиента

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\ProductSubscribesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$delete_product_subscribes_request = new \Ensi\CrmClient\Dto\DeleteProductSubscribesRequest(); // \Ensi\CrmClient\Dto\DeleteProductSubscribesRequest | 

try {
    $result = $apiInstance->deleteProductFromProductSubscribes($delete_product_subscribes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductSubscribesApi->deleteProductFromProductSubscribes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delete_product_subscribes_request** | [**\Ensi\CrmClient\Dto\DeleteProductSubscribesRequest**](../Model/DeleteProductSubscribesRequest.md)|  |

### Return type

[**\Ensi\CrmClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchProductSubscribes

> \Ensi\CrmClient\Dto\SearchProductSubscribesResponse searchProductSubscribes($search_product_subscribes_request)

Поиск объектов типа ProductSubscribe

Поиск объектов типа ProductSubscribe

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\ProductSubscribesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_product_subscribes_request = new \Ensi\CrmClient\Dto\SearchProductSubscribesRequest(); // \Ensi\CrmClient\Dto\SearchProductSubscribesRequest | 

try {
    $result = $apiInstance->searchProductSubscribes($search_product_subscribes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductSubscribesApi->searchProductSubscribes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_product_subscribes_request** | [**\Ensi\CrmClient\Dto\SearchProductSubscribesRequest**](../Model/SearchProductSubscribesRequest.md)|  |

### Return type

[**\Ensi\CrmClient\Dto\SearchProductSubscribesResponse**](../Model/SearchProductSubscribesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

