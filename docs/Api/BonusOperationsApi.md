# Ensi\CrmClient\BonusOperationsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createBonusOperation**](BonusOperationsApi.md#createBonusOperation) | **POST** /customers/bonus-operations | Создание бонусной операции
[**deleteBonusOperation**](BonusOperationsApi.md#deleteBonusOperation) | **DELETE** /customers/bonus-operations/{id} | Удаление бонусной операции
[**getBonusOperation**](BonusOperationsApi.md#getBonusOperation) | **GET** /customers/bonus-operations/{id} | Получение бонусной операции
[**patchBonusOperation**](BonusOperationsApi.md#patchBonusOperation) | **PATCH** /customers/bonus-operations/{id} | Обновление полей бонусной операции
[**searchBonusOperations**](BonusOperationsApi.md#searchBonusOperations) | **POST** /customers/bonus-operations:search | Поиск бонусных операций



## createBonusOperation

> \Ensi\CrmClient\Dto\BonusOperationResponse createBonusOperation($create_bonus_operation_request)

Создание бонусной операции

Создание бонусной операции

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\BonusOperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_bonus_operation_request = new \Ensi\CrmClient\Dto\CreateBonusOperationRequest(); // \Ensi\CrmClient\Dto\CreateBonusOperationRequest | 

try {
    $result = $apiInstance->createBonusOperation($create_bonus_operation_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BonusOperationsApi->createBonusOperation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_bonus_operation_request** | [**\Ensi\CrmClient\Dto\CreateBonusOperationRequest**](../Model/CreateBonusOperationRequest.md)|  |

### Return type

[**\Ensi\CrmClient\Dto\BonusOperationResponse**](../Model/BonusOperationResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteBonusOperation

> \Ensi\CrmClient\Dto\EmptyDataResponse deleteBonusOperation($id)

Удаление бонусной операции

Удаление бонусной операции

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\BonusOperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteBonusOperation($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BonusOperationsApi->deleteBonusOperation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CrmClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getBonusOperation

> \Ensi\CrmClient\Dto\BonusOperationResponse getBonusOperation($id)

Получение бонусной операции

Получение бонусной операции

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\BonusOperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->getBonusOperation($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BonusOperationsApi->getBonusOperation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CrmClient\Dto\BonusOperationResponse**](../Model/BonusOperationResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchBonusOperation

> \Ensi\CrmClient\Dto\BonusOperationResponse patchBonusOperation($id, $patch_bonus_operation_request)

Обновление полей бонусной операции

Обновление полей бонусной операции

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\BonusOperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_bonus_operation_request = new \Ensi\CrmClient\Dto\PatchBonusOperationRequest(); // \Ensi\CrmClient\Dto\PatchBonusOperationRequest | 

try {
    $result = $apiInstance->patchBonusOperation($id, $patch_bonus_operation_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BonusOperationsApi->patchBonusOperation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_bonus_operation_request** | [**\Ensi\CrmClient\Dto\PatchBonusOperationRequest**](../Model/PatchBonusOperationRequest.md)|  |

### Return type

[**\Ensi\CrmClient\Dto\BonusOperationResponse**](../Model/BonusOperationResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchBonusOperations

> \Ensi\CrmClient\Dto\SearchBonusOperationsResponse searchBonusOperations($search_bonus_operations_request)

Поиск бонусных операций

Поиск бонусных операций

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\BonusOperationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_bonus_operations_request = new \Ensi\CrmClient\Dto\SearchBonusOperationsRequest(); // \Ensi\CrmClient\Dto\SearchBonusOperationsRequest | 

try {
    $result = $apiInstance->searchBonusOperations($search_bonus_operations_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BonusOperationsApi->searchBonusOperations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_bonus_operations_request** | [**\Ensi\CrmClient\Dto\SearchBonusOperationsRequest**](../Model/SearchBonusOperationsRequest.md)|  |

### Return type

[**\Ensi\CrmClient\Dto\SearchBonusOperationsResponse**](../Model/SearchBonusOperationsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

