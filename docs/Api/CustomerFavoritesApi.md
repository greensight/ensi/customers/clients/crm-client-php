# Ensi\CrmClient\CustomerFavoritesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**clearCustomerFavorites**](CustomerFavoritesApi.md#clearCustomerFavorites) | **POST** /customers/favorites:clear | Удаление всех товаров из списка избранного клиента
[**createCustomerFavorites**](CustomerFavoritesApi.md#createCustomerFavorites) | **POST** /customers/favorites:add-products | Массовое создание объектов типа CustomerFavorite
[**deleteCustomerFavorites**](CustomerFavoritesApi.md#deleteCustomerFavorites) | **POST** /customers/favorites:delete-products | Массовое удаление объектов типа CustomerFavorite
[**searchCustomerFavorite**](CustomerFavoritesApi.md#searchCustomerFavorite) | **POST** /customers/favorites:search | Поиск объектов типа CustomerFavorite



## clearCustomerFavorites

> \Ensi\CrmClient\Dto\EmptyDataResponse clearCustomerFavorites($clear_customer_favorites_request)

Удаление всех товаров из списка избранного клиента

Удаление всех товаров из списка избранного клиента

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\CustomerFavoritesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$clear_customer_favorites_request = new \Ensi\CrmClient\Dto\ClearCustomerFavoritesRequest(); // \Ensi\CrmClient\Dto\ClearCustomerFavoritesRequest | 

try {
    $result = $apiInstance->clearCustomerFavorites($clear_customer_favorites_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerFavoritesApi->clearCustomerFavorites: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clear_customer_favorites_request** | [**\Ensi\CrmClient\Dto\ClearCustomerFavoritesRequest**](../Model/ClearCustomerFavoritesRequest.md)|  |

### Return type

[**\Ensi\CrmClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## createCustomerFavorites

> \Ensi\CrmClient\Dto\EmptyDataResponse createCustomerFavorites($create_customer_favorites_request)

Массовое создание объектов типа CustomerFavorite

Массовое создание объектов типа CustomerFavorite

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\CustomerFavoritesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_customer_favorites_request = new \Ensi\CrmClient\Dto\CreateCustomerFavoritesRequest(); // \Ensi\CrmClient\Dto\CreateCustomerFavoritesRequest | 

try {
    $result = $apiInstance->createCustomerFavorites($create_customer_favorites_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerFavoritesApi->createCustomerFavorites: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_customer_favorites_request** | [**\Ensi\CrmClient\Dto\CreateCustomerFavoritesRequest**](../Model/CreateCustomerFavoritesRequest.md)|  |

### Return type

[**\Ensi\CrmClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteCustomerFavorites

> \Ensi\CrmClient\Dto\EmptyDataResponse deleteCustomerFavorites($delete_customer_favorites_request)

Массовое удаление объектов типа CustomerFavorite

Массовое удаление объектов типа CustomerFavorite

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\CustomerFavoritesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$delete_customer_favorites_request = new \Ensi\CrmClient\Dto\DeleteCustomerFavoritesRequest(); // \Ensi\CrmClient\Dto\DeleteCustomerFavoritesRequest | 

try {
    $result = $apiInstance->deleteCustomerFavorites($delete_customer_favorites_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerFavoritesApi->deleteCustomerFavorites: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delete_customer_favorites_request** | [**\Ensi\CrmClient\Dto\DeleteCustomerFavoritesRequest**](../Model/DeleteCustomerFavoritesRequest.md)|  |

### Return type

[**\Ensi\CrmClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchCustomerFavorite

> \Ensi\CrmClient\Dto\SearchCustomerFavoritesResponse searchCustomerFavorite($search_customer_favorites_request)

Поиск объектов типа CustomerFavorite

Поиск объектов типа CustomerFavorite

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CrmClient\Api\CustomerFavoritesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_customer_favorites_request = new \Ensi\CrmClient\Dto\SearchCustomerFavoritesRequest(); // \Ensi\CrmClient\Dto\SearchCustomerFavoritesRequest | 

try {
    $result = $apiInstance->searchCustomerFavorite($search_customer_favorites_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerFavoritesApi->searchCustomerFavorite: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_customer_favorites_request** | [**\Ensi\CrmClient\Dto\SearchCustomerFavoritesRequest**](../Model/SearchCustomerFavoritesRequest.md)|  |

### Return type

[**\Ensi\CrmClient\Dto\SearchCustomerFavoritesResponse**](../Model/SearchCustomerFavoritesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

