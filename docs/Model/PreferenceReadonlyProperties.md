# # PreferenceReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор | 
**customer_id** | **int** | Идентификатор клиента | 
**attribute_name** | **string** | Название атрибута | 
**attribute_value** | **string** | Значение атрибута атрибута | 
**product_count** | **int** | Колличество купленных товаров | 
**product_sum** | **int** | Cумма стоимости купленных товаров | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


