# # SearchCustomersInfoResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Ensi\CrmClient\Dto\CustomerInfo[]**](CustomerInfo.md) |  | 
**meta** | [**\Ensi\CrmClient\Dto\SearchCustomersInfoResponseMeta**](SearchCustomersInfoResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


